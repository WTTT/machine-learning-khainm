import numpy as np 
import matplotlib.pyplot as plt

np.random.seed(2)

# dữ liệu khởi điểm để test thuật toán
means = [[2, 2], [4, 2]]
cov = [[.3, .2], [.2, .3]]
N = 10
X0 = np.random.multivariate_normal(means[0], cov, N).T # các điểm với nhãn 1
X1 = np.random.multivariate_normal(means[1], cov, N).T # các điểm với nhãn -1

X = np.concatenate((X0, X1), axis = 1)
y = np.concatenate((np.ones((1, N)), -1*np.ones((1, N))), axis = 1)
# tạo Xbar 
X = np.concatenate((np.ones((1, 2*N)), X), axis = 0)

# đữa ra dấu để sau đó so sánh với nhãn y gốc
def h(w, x):    
    return np.sign(np.dot(w.T, x))

# so sánh với nahxn y gốc
def has_converged(X, y, w):    
    return np.array_equal(h(w, X), y) 

# PLA chính
def perceptron(X, y, w_init):
    w = [w_init]
    N = X.shape[1]
    d = X.shape[0]
    mis_points = []
    while True:
        # mix data 
        mix_id = np.random.permutation(N)
        for i in range(N):
            xi = X[:, mix_id[i]].reshape(d, 1)
            yi = y[0, mix_id[i]]
            if h(w[-1], xi)[0] != yi: # đây là điểm phân loại sai
                mis_points.append(mix_id[i])
                w_new = w[-1] + yi*xi 
                w.append(w_new)
                
        if has_converged(X, y, w[-1]):
            break
    return (w, mis_points)

d = X.shape[0]
w_init = np.random.randn(d, 1)
(w, m) = perceptron(X, y, w_init)

# minh họa

x1 = 0
y1 = - (x1 * w[-1][1][0] + w[-1][0][0]) / w[-1][2][0]
x2 = 8
y2 = - (x2 * w[-1][1][0] + w[-1][0][0]) / w[-1][2][0]
plt.plot(X0[0, :], X0[1, :], "b*")
plt.plot(X1[0, :], X1[1, :], "r^")
plt.plot([x1, x2], [y1, y2])
plt.axis([0, 7, 0, 7])
plt.show()


