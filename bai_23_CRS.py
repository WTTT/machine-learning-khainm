import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.linear_model import Ridge
from sklearn import linear_model
import math

# đọc file
u_cols =  ['user_id', 'age', 'sex', 'occupation', 'zip_code']
users = pd.read_csv('C:/Users/ADMIN/Desktop/Project/GitLab/machine-learning-khainm/ml-100k/u.user', sep='|', names=u_cols,
 encoding='UTF-8')

n_users = users.shape[0]

# đọc dữ liệu
r_cols = ['user_id', 'movie_id', 'rating', 'unix_timestamp']

rating_base = pd.read_csv("C:/Users/ADMIN/Desktop/Project/GitLab/machine-learning-khainm/ml-100k/ua.base", sep = "\t", names = r_cols, encoding = "UTF-8")
rating_test = pd.read_csv("C:/Users/ADMIN/Desktop/Project/GitLab/machine-learning-khainm/ml-100k/ua.test", sep = "\t", names = r_cols, encoding = "UTF-8")

# MATRIX
rate_train = np.asarray(rating_base)
rate_test = np.asarray(rating_test)

print("\nSố train: " + str(rate_train.shape[0]))
print("\nSố test: " + str(rating_test.shape[0]))

# Xây dựng profile items
item_cols = ["movie id", "movie title", "release date", "IMDB URL", 'unknown', 'Action', 'Adventure',
 'Animation', 'Children\'s', 'Comedy', 'Crime', 'Documentary', 'Drama', 'Fantasy',
 'Film-Noir', 'Horror', 'Musical', 'Mystery', 'Romance', 'Sci-Fi', 'Thriller', 'War', 'Western']

items = pd.read_csv("C:/Users/ADMIN/Desktop/Project/GitLab/machine-learning-khainm/ml-100k/u.item", sep = "|", names = item_cols, encoding = "latin-1")

# Thông tin làm việc
X0 = np.asarray(items)
X_train_counts = X0[:, -19:]

# sklearn xử lý thông tin 
transformer = TfidfTransformer(smooth_idf=True, norm ='l2')
tfidf = transformer.fit_transform(X_train_counts.tolist()).toarray()

# xây dựng mô hình dự đoán cho user n
def get_items_rated_by_user(rate_matrix, user_id):
    y = rate_matrix[:,0] 
    ids = np.where(y == user_id +1)[0] 
    item_ids = rate_matrix[ids, 1] - 1
    scores = rate_matrix[ids, 2]
    return (item_ids, scores)

d = tfidf.shape[1]
W = np.zeros((d, n_users))
b = np.zeros((1, n_users))

for n in range(n_users):    
    ids, scores = get_items_rated_by_user(rate_train, n)
    clf = Ridge(alpha=0.01, fit_intercept  = True)
    Xhat = tfidf[ids, :]
    
    clf.fit(Xhat, scores) 
    W[:, n] = clf.coef_
    b[0, n] = clf.intercept_


# dự đoán
Yhat = tfidf.dot(W) + b
n = 10
np.set_printoptions(precision=2)  
ids, scores = get_items_rated_by_user(rate_test, n)
Yhat[n, ids]
print('Rated movies ids :', ids )
print('True ratings     :', scores)
print('Predicted ratings:', Yhat[ids, n])

# kiểm tra sai số
def evaluate(Yhat, rates, W, b):
    se = 0
    cnt = 0
    for n in range(n_users):
        ids, scores_truth = get_items_rated_by_user(rates, n)
        scores_pred = Yhat[ids, n]
        e = scores_truth - scores_pred 
        se += (e*e).sum(axis = 0)
        cnt += e.size 
    return math.sqrt(se/cnt)

print('Sai số cúa tập huấn luyện: ', evaluate(Yhat, rate_train, W, b))
print('Sai số của tập test: ', evaluate(Yhat, rate_test, W, b))


