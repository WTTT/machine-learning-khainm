import numpy as np
np.random.seed(1)
# TRAINING DATA
X = np.array([[0, 0, 1],[1, 1, 1], [1, 0, 1], [0, 1, 1]])
Y = np.array([[0, 1, 1, 0]]).T

# signmoid funtions    
def nonlin(x, der = False):
    if(der):
        return x * (1 - x)                                                                                                                                                
    else:
        return 1 / (1 + np.exp(-x))

#weight 
syn0 = 2 * np.random.random((3, 1)) - 1

for i in range(10000):
    layer0 = X
    layer1 = nonlin(np.dot(layer0, syn0))
    layer1_error = Y - layer1
    layer1_delta = layer1_error * nonlin(layer1, True)
    syn0 += np.dot(layer0.T, layer1_delta)

# Predict 
Xtest = np.array([[1, 1, 1]])
print(nonlin(np.dot(Xtest, syn0)))
