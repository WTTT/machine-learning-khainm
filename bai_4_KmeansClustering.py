from __future__ import print_function 
import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial.distance import cdist

np.random.seed(11)

# khởi tạo dãy dữ liệu để kiểm tra Kmeans Clustering
means = np.array([[2, 2], [8, 3], [3, 6]]) # centers gốc để tạo dữ liệu
cov = np.array([[1, 0], [0, 1]]) # ma trận hiệp phuong sai -- ma trận đơn vị
# tạo dữ liệu
N = 500 # số lượng dữ liệu có của 1 cluster khởi tạo
K = 3 # số cluster mon muốn
X0 = np.random.multivariate_normal(means[0], cov, N)
X1 = np.random.multivariate_normal(means[1], cov, N)
X2 = np.random.multivariate_normal(means[2], cov, N)

X = np.concatenate((X0, X1, X2), axis = 0)

original_labels = np.asarray([0] * N + [1] * N + [2] * N) # tại thời điểm đầu đây là label của mỗi điểm dữ liệu

# hiển thị
plt.plot(X0[:, 0], X0[:, 1], 'ro')
plt.plot(X1[:, 0], X1[:, 1], 'b*')
plt.plot(X2[:, 0], X2[:, 1], 'ks')


plt.plot(means[:, 0], means[:, 1], 'yo', markersize = 10) # centers gốc

plt.xlabel('X')
plt.ylabel('Y')

plt.axis('equal')
plt.show()

# Hàm
# Hiển Thị
def kmeans_display(X, labels, centers):
    K = np.amax(labels) + 1
    X0 = X[labels == 0, :]
    X1 = X[labels == 1, :]
    X2 = X[labels == 2, :]

    plt.plot(X0[:, 0], X0[:, 1], 'ro')
    plt.plot(X1[:, 0], X1[:, 1], 'b*')
    plt.plot(X2[:, 0], X2[:, 1], 'ks')

    plt.plot(centers[:, 0], centers[:, 1], 'yo', markersize = 10)

    plt.show()

# Khởi tạo centers ban đầu có chuẩn kì vọng
def kmeans_init_centers(X, K):
    return X[np.random.choice(X.shape[0], K, replace = False)]

# Dán nhãn các điểm khi biết center

def kmeans_assign_labels(X, centers):
    # tìm khoảng cách
    D = cdist(X, centers)
    return np.argmin(D, axis = 1)

def kmeans_update_centers(X, labels, K):
    centers = np.ones((K, X.shape[1]))
    for k in range(K):
        Xk = X[labels == k, :] # lấy những điểm thuộc k cluster
        centers[k, :] = np.mean(Xk, axis = 0) # lấy trung bình của Xk
    return centers

def has_converged(centers, new_centers):
    return (set([tuple(a) for a in centers]) == 
        set([tuple(a) for a in new_centers]))

def kmeans_clustering(X, K):
    centers = [kmeans_init_centers(X, K)]
    labels = []
    it = 0 # đếm số lần chạy chương trình
    while True:
        labels.append(kmeans_assign_labels(X, centers[-1]))
        new_centers = kmeans_update_centers(X, labels[-1], K)
        if has_converged(centers[-1], new_centers):
            break
        centers.append(new_centers)
        it += 1
    return (centers, labels, it)

(centers, labels, it) = kmeans_clustering(X, K)
kmeans_display(X, labels[-1], centers[-1])