import pandas as pd
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from scipy import sparse

# DATA
r_cols = ["user_id", "item_id", "rating"]
ratings = pd.read_csv("C:/Users/ADMIN/Desktop/Project/GitLab/machine-learning-khainm/ex.txt", sep = " ", names = r_cols)
Y_data = np.array(ratings)

# CLASS
class CF(object):
    def __init__(self, Y_data, k, dist_func = cosine_similarity, uuCF = 1):
        self.uuCF = uuCF # để xét theo user-user hay item-item
        self.Y_data = Y_data if uuCF else Y_data[:, [1, 0, 2]] # sắp lại nếu theo item-item
        self.k = k # số lân cận muốn xét
        self.dist_func = dist_func
        self.Ybar_data = None
        self.n_users = int(np.max(self.Y_data[:, 0])) + 1
        self.n_items = int(np.max(self.Y_data[:, 1])) + 1

    def add(self, new_data):
        self.Y_data = np.concatenate((self.Y_data, new_data), axis = 0)

    def normalize_Y(self):
        users = self.Y_data[:, 0] 
        self.Ybar_data = self.Y_data.copy()
        self.mu = np.zeros((self.n_users,))
        for n in range(self.n_users):
            ids = np.where(users == n)[0].astype(np.int32)
            item_ids = self.Y_data[ids, 1] 
            ratings = self.Y_data[ids, 2]
            m = np.mean(ratings) 
            if np.isnan(m):
                m = 0
            self.Ybar_data[ids, 2] = ratings - self.mu[n]

        self.Ybar = sparse.coo_matrix((self.Ybar_data[:, 2],
            (self.Ybar_data[:, 1], self.Ybar_data[:, 0])), (self.n_items, self.n_users))
        self.Ybar = self.Ybar.tocsr()

    def similarity(self):
        self.S = self.dist_func(self.Ybar.T, self.Ybar.T)

    def refresh(self):
        self.normalize_Y()
        self.similarity() 
        
    def fit(self):
        self.refresh()

    def __pred(self, u, i, normalized = 1):
        ids = np.where(self.Y_data[:, 1] == i)[0].astype(np.int32)
        users_rated_i = (self.Y_data[ids, 0]).astype(np.int32)
        sim = self.S[u, users_rated_i]
        a = np.argsort(sim)[-self.k:] 
        nearest_s = sim[a]
        r = self.Ybar[i, users_rated_i[a]]
        if normalized:
            return (r*nearest_s)[0]/(np.abs(nearest_s).sum() + 1e-8)
        return (r*nearest_s)[0]/(np.abs(nearest_s).sum() + 1e-8) + self.mu[u]
    
    
    def pred(self, u, i, normalized = 1):
        if self.uuCF: return self.__pred(u, i, normalize)
        return self.__pred(i, u, normalize)

    def recommend(self, u, normalized = 1):
        ids = np.where(self.Y_data[:, 0] == u)[0]
        items_rated_by_u = self.Y_data[ids, 1].tolist()              
        recommended_items = []
        for i in range(self.n_items):
            if i not in items_rated_by_u:
                rating = self.__pred(u, i)
                if rating > 0: 
                    recommended_items.append(i)
        
        return recommended_items 
    
    def print_recommendation(self):
        print("Gợi ý")
        for u in range(self.n_users):
            recommended_items = self.recommend(u)
            if self.uuCF:
                print("Gợi ý items: " +  str(recommended_items) +  " cho người dùng: " + str(u))
            else: 
                print("Gợi ý item: " + str(u) + " cho người dùng: " + str(recommended_items))

# chạy chương trình
# theo user-user
print("\nTheo người dùng:")
rs = CF(Y_data, k = 2, uuCF = 1)
rs.fit()
rs.print_recommendation()
# theo item-item
print("___________________________________________")
print("\nTheo item:")
rs = CF(Y_data, k = 2, uuCF = 0)
rs.fit()
rs.print_recommendation()